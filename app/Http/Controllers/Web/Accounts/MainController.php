<?php

namespace App\Http\Controllers\Web\Accounts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.accounts.sub-pages.landing');
    }

    public function indexNewAccount()
    {
        return view('pages.accounts.sub-pages.new-account');
    }
}
