<?php

namespace App\Http\Controllers\Web\Activities;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        return view('pages.activities.sub-pages.manage-categories');
    }
}
