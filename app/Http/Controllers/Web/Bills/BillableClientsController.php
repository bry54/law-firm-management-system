<?php

namespace App\Http\Controllers\Web\Bills;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BillableClientsController extends Controller
{
    public function index()
    {
        return view('pages.bills.sub-pages.billable-clients');
    }
}
