<?php

namespace App\Http\Controllers\Web\Bills;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.bills.sub-pages.bills');
    }

    public function indexRecordPayment()
    {
        return view('pages.bills.sub-pages.record-payment');
    }
}
