<?php

namespace App\Http\Controllers\Web\Bills;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class StatementOfAccountsController extends Controller
{
    public function index()
    {
        return view('pages.bills.sub-pages.statement-of-accounts');
    }
}
