<?php

namespace App\Http\Controllers\Web\Calendar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.calendar.sub-pages.landing');
    }

    public function indexNewEvent()
    {
        return view('pages.calendar.sub-pages.new-event');
    }
}
