<?php

namespace App\Http\Controllers\Web\Contacts;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.contacts.sub-pages.landing');
    }

    public function indexNewPerson()
    {
        return view('pages.contacts.sub-pages.new-person');
    }

    public function indexNewCompany()
    {
        return view('pages.contacts.sub-pages.new-company');
    }
}
