<?php

namespace App\Http\Controllers\Web\Documents;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.documents.sub-pages.all-documents');
    }

    public function indexCategories()
    {
        return view('pages.documents.sub-pages.categories');
    }
}
