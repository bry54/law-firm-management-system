<?php

namespace App\Http\Controllers\Web\Matters;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.matters.sub-pages.landing');
    }

    public function indexNewMatter()
    {
        return view('pages.matters.sub-pages.new-matter');
    }
}
