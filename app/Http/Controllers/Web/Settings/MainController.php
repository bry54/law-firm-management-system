<?php

namespace App\Http\Controllers\Web\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.settings.sub-pages.landing');
    }

    public function indexProfileEdit()
    {
        return view('pages.settings.sub-pages.profile-edit');
    }

    public function indexPractiseArea()
    {
        return view('pages.settings.sub-pages.practice-areas');
    }

    public function indexBilling()
    {
        return view('pages.settings.sub-pages.billing-settings');
    }

    public function indexUsers()
    {
        return view('pages.settings.sub-pages.manage-users');
    }

    public function indexGroupPermissions()
    {
        return view('pages.settings.sub-pages.groups-permissions');
    }
}
