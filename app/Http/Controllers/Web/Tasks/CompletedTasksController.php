<?php

namespace App\Http\Controllers\Web\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CompletedTasksController extends Controller
{
    public function index()
    {
        return view('pages.tasks.sub-pages.completed-tasks');
    }
}
