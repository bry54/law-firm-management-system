<?php

namespace App\Http\Controllers\Web\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index()
    {
        return view('pages.tasks.sub-pages.new-task');
    }
}
