<?php

namespace App\Http\Controllers\Web\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpcomingTasksController extends Controller
{
    public function index()
    {
        return view('pages.tasks.sub-pages.upcoming-tasks');
    }
}
