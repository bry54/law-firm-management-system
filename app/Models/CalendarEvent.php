<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarEvent extends Model
{
    protected $fillable = [
        'name', 'start_time', 'end_time', 'location', 'matter', 'description', 'attendees', 'is_repeating'
    ];
}
