<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Matter extends Model
{
    protected $fillable = [
        'client', 'description', 'responsible_solicitor', 'originating_solicitor', 'client_reference_number', 'location', 'permissions',
        'practice_area', 'status', 'open_date', 'closed_date', 'pending_date', 'limitations_date', 
    ];
}
