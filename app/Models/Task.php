<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = [
        'name', 'description', 'assignee_type', 'assignee', 'matter', 'due_date', 'priority', 'task_type', 'status', 'time_estimate'
    ];
}
