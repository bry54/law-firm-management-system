<?php

return [
    'menu_title' => 'Accounts',
    'btn_export_transactions' => 'Export Transactions',
    'btn_new_account' => 'New Account',
    'tbl_acc_name' => 'Account Name',
    'tbl_currency'=>'Currency',
    'tbl_balance'=>'Balance',
    'tbl_default'=>'Default Account',
    'tbl_actions'=>'Actions',
];
