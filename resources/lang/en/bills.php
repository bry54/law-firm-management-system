<?php

return [
    'menu_title' => 'Bills',
    'btn_record_payment' => 'Record Payment',
    'btn_new_trust' => 'New Trust Request',
    'btn_all' => 'All Bills',
    'btn_billable' => 'Billable Clients',
    'btn_outstanding' => 'Outstanding Balances',
    'btn_statement' => 'Statement',
    'btn_export' => 'Export Transactions',

    'btn_grp_all' => 'All',
    'btn_grp_drafts' => 'Drafts',
    'btn_grp_paid' => 'Paid',
    'btn_grp_pending' => 'Pending Approval',
    'btn_grp_awaiting' => 'Awaiting Payment',

    'lbl_all_bills' => 'All Bills',
    'lbl_billable_clients' => 'Billable Clients',
    'lbl_export_transactions' => 'Export Transactions',
    'lbl_outstanding_balance' => 'Outstanding Balances',
    'lbl_statement_of_accounts' => 'Statement Of Accounts',

    'tbl_client'=>'Client',
    'tbl_unbilled_hours'=>'Un-billed Hours',
    'tbl_amt_due'=>'Amount Due',
    'tbl_amt_in_trust'=>'Amount In Trust',
    'tbl_actions'=>'Actions',

    'tbl_status'=>'Status',
    'tbl_matters'=>'Matter (s)',
    'tbl_due_date'=>'Due In',
    'tbl_issue_date'=>'Issue Date',
    'tbl_balance'=>'Balance',
    'tbl_paid_on'=>'Paid On',
    'tbl_amt_paid'=>'Amt Paid',
    'tbl_type'=>'Type',
    'tbl_total'=>'Total',
    'tbl_due_in'=>'Due In',

    'tbl_last_paid'=>'Last Paid',
    'tbl_last_new_bill_due_date'=>'Newest Bill\'s Due Date',
    'tbl_last_shared'=>'Last Shared',
];
