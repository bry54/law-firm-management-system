<?php

return [
    'menu_title' => 'Contacts',
    'btn_export' => 'Export',
    'btn_all' => 'All',
    'btn_people' => 'People',
    'btn_companies' => 'Companies',
    'btn_new_company' => 'New Company',
    'btn_new_person' => 'New Person',
    'lbl_choose' => 'Choose Action',
    'lbl_print' => 'Print',
    'lbl_excel' => 'Excel',

    'tbl_type' => 'Type',
    'tbl_name' => 'Name',
    'tbl_phone' => 'Phone',
    'tbl_email' => 'Email',
    'tbl_address' => 'Address',
    'tbl_actions' => 'Actions',
];
