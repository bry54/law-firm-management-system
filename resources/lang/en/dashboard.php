<?php

return [
    'menu_title' => 'Dashboard',
    'agenda' => [
        'main_header' => 'Today\'s Agenda',
        'tasks_title' => 'Tasks Due Today',
        'btn_new_task' => 'New Task',
        'calendar_title' => 'Calendar Events',
        'btn_new_event' => 'New Event',
    ],

    'metrics' => [
        'main_header' => 'Firm Metrics',
        'billable_hours_title' => 'Billable Hours Target',
        'btn_setup_target' => 'Setup Targets',
        'lbl_no_billing_target' => 'You haven\'t set up your billing target.',
        'bill_overview_title' => 'Bills Overview',

        'nav_bill_draft_title' => 'Draft Bills',
        'nav_bill_draft_subtitle' => 'section extract',
        'nav_bill_draft_total_title' => 'Total In Drafts',
        'nav_bill_draft_total_subtitle' => 'section extract',

        'nav_bill_awaiting_payment_title' => 'Bills Awaiting Payment',
        'nav_bill_awaiting_payment_subtitle' => 'section extract',
        'nav_bill_awaiting_payment_total_title' => 'Total In Awaiting Payments',
        'nav_bill_awaiting_payment_total_subtitle' => 'section extract',


        'nav_bill_overdue_title' => 'Overdue Bills',
        'nav_bill_overdue_subtitle' => 'section extract',
        'nav_bill_overdue_total_title' => 'Total In Overdue',
        'nav_bill_overdue_total_subtitle' => 'section extract',
    ]
];
