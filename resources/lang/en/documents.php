<?php

return [
    'menu_title' => 'Documents',
    'btn_all_docs' => 'All Documents',
    'btn_categories' => 'Categories',
    'btn_templates' => 'Templates',
    'btn_new_category' => 'New Category',
    'tbl_actions' => 'Actions',

    'tbl_category_name' => 'Category Name',
];
