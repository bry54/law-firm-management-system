<?php

return [
    'corporate_section' => 'Corporate Functions',

    'showcase' => [
        'title' => 'Law Firm Management Software',
        'subtitle' => 'The ultimate software solution for running a law firm'
    ],

    'user' => [
        'hie' => 'Hi, ',
        'profile_title' => 'My Profile',
        'profile_sub' => 'Account settings and more...',
        'message_title' => 'Messages',
        'message_sub' => 'View your secure inbox',
        'sign_out'=> 'Sign Out'
    ],

    'lang' => [
        'choose' => 'Select your prefered language',
        'english' => 'English',
        'turkish' => 'Turkish',
        'greek' => 'Greek',
    ],

    'actions' => [
        'title' => 'Quick User Actions',
        'accounting_title' => 'Accounting',
        'accounting_sub' => 'eCommerce',
    ],

    'footer' => [
        'team' => 'Team',
        'about' => 'About',
        'contact' => 'Contact',
        'copyright' => date("Y").' @ :company',
    ]
];
