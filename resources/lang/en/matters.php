<?php

return [
    'menu_title' => 'Matters',
    'btn_new_matter' => 'New Matter',
    'btn_all' => 'All',
    'btn_open' => 'Open',
    'btn_pending' => 'Pending',
    'btn_closed' => 'Closed',

    'btn_export' => 'Export',
    'lbl_choose' => 'Choose Action',
    'lbl_print' => 'Print',
    'lbl_excel' => 'Excel',

    'tbl_matter' => 'Matter',
    'tbl_client' => 'Client',
    'tbl_responsible_solicitor' => 'responsible Solicitor',
    'tbl_originating_solicitor' => 'Originating Solicitor',
    'tbl_practise_area' => 'Practise Area',
    'tbl_open_date' => 'Open Date',
    'tbl_actions' => 'Actions',
];
