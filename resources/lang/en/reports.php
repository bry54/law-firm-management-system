<?php

return [
    'menu_title' => 'Reports',
    'billing_reports' => [
        'heading' => 'Billing reports',

        'acc_receivable_title' => 'Accounts Receivables',
        'acc_receivable_subtitle' => 'some information about report',

        'aging_acc_receivable_title' => 'Accounts Receivable Aging',
        'aging_acc_receivable_subtitle' => 'some information about report',

        'billing_history_title' => 'Billing History',
        'billing_history_subtitle' => 'some information about report',

        'matter_balance_title' => 'Matter Balance Summary',
        'matter_balance_subtitle' => 'some information about report',

        'invoice_payments_title' => 'Invoice Payments Reports',
        'invoice_payments_subtitle' => 'some information about report',
    ],

    'client_reports' => [
        'heading' => 'Client reports',

        'client_activity_title' => 'Client Activity',
        'client_activity_subtitle' => 'some information about report',

        'client_ledger_title' => 'Client Ledger',
        'client_ledger_subtitle' => 'some information about report',

        'trust_ledger_title' => 'Trust Ledger',
        'trust_ledger_subtitle' => 'some information about report',

        'trust_management_title' => 'Trust Management',
        'trust_management_subtitle' => 'some information about report',

        'trust_listing_title' => 'Trust Listing',
        'trust_listing_subtitle' => 'some information about report',

        'work_in_progress_title' => 'Work In Progress',
        'work_in_progress_subtitle' => 'some information about report',

        'bank_account_activity_title' => 'Bank Account Activity',
        'bank_account_activity_subtitle' => 'some information about report',
    ],

    'matter_reports' => [
        'heading' => 'Matter reports',

        'matters_title' => 'Matters',
        'matters_subtitle' => 'some information about report',

        'matters_by_responsible_authorities_title' => 'Matters By Responsible Authorities',
        'matters_by_responsible_authorities_subtitle' => 'some information about report',

        'contact_information_title' => 'Contact Information',
        'contact_information_subtitle' => 'some information about report',
    ],

    'productivity_reports' => [
        'heading' => 'Productivity Reports',

        'productivity_by_client_title' => 'Productivity By Client',
        'productivity_by_client_subtitle' => 'some information about report',

        'productivity_by_user_title' => 'Productivity By User',
        'productivity_by_user_subtitle' => 'some information about report',
    ],

    'revenue_reports' => [
        'heading' => 'Revenue Reports',

        'revenue_report_title' => 'Revenue Report',
        'revenue_report_subtitle' => 'some information about report',
    ],
];
