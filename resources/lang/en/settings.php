<?php

return [
    'menu_title' => 'Settings',

    'system_settings' => [
        'heading' => 'System Settings',

        'acc_payment_info_title' => 'Account and Payment Information',
        'acc_payment_info_subtitle' => 'Some information here...',

        'manage_users_title' => 'Manage Users',
        'manage_users_subtitle' => 'Some information here...',

        'groups_permissions_title' => 'Groups, Permissions and Titles',
        'groups_permissions_subtitle' => 'Some information here...',

        'security_title' => 'Security',
        'security_subtitle' => 'Some information here...',

        'btn_new_user' => 'New User',
        'tbl_title' => 'Title',
        'tbl_fullname' => 'Full Name',
        'tbl_groups' => 'Groups',
        'tbl_email' => 'email',
        'tbl_permissions' => 'Permissions',
        'tbl_billing_rate' => 'Billing Rate',
        'tbl_actions' => 'Actions',
    ],

    'user_settings' => [
        'heading' => 'Personal Settings',

        'profile_title' => 'Edit Profile',
        'profile_subtitle' => 'Some information here...',

        'tab_mail_drop_alias' => 'Mail Drop Aliases',
        'tab_profile_pic' => 'Profile Picture',
        'tab_change_login' => 'Change Login Credentials',
        'tab_your_info' => 'Your Information'
    ],

    'firm_settings' => [
        'heading' => 'Firm Settings',

        'practise_title' => 'Practise Areas',
        'practise_subtitle' => 'Some information here...',
        'btn_new_practice' => 'New Practice Area',

        'billing_title' => 'Billing',
        'billing_subtitle' => 'Some information here...',

        'tbl_practice_name'=>'Practice Area',
        'tbl_actions'=>'Actions',
    ]
];
