<?php

return [
    'menu_title' => 'Tasks',
    'btn_new_task' => 'New Task',
    'btn_upcoming' => 'Upcoming',
    'btn_completed' => 'Completed',

    'btn_grp_all' => 'Completed',
    'btn_grp_this_wk' => 'Due This Week',
    'btn_grp_today' => 'Due Today',
    'btn_grp_tomorrow' => 'Due Tomorrow',
    'btn_grp_overdue' => 'Overdue',

    'title_completed_tasks' => 'Completed Tasks',
    'title_upcoming_tasks' => 'Upcoming Tasks',
    'tbl_due_date' => 'Due Date',
    'tbl_task' => 'Task',
    'tbl_description' => 'Description',
    'tbl_matter' => 'Matter (s)',
    'tbl_actions' => 'Actions',
];
