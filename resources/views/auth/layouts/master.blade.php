<!DOCTYPE html>
<html lang="{{ Config::get('app.locale') }}">

<!-- begin::Head -->
<head>
    <title>{{ config('app.name', 'Law Company') }} | @yield('form-title')</title>
    <meta name="description" content="Login page example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{ asset('assets/css/pages/login/login-1.css') }}" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <link href="{{ asset('assets/plugins/general/sweetalert2/dist/sweetalert2.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/general/socicon/css/socicon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/general/plugins/line-awesome/css/line-awesome.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/general/plugins/flaticon/flaticon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/general/plugins/flaticon2/flaticon.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />

    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />

    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}"/>
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">

            <!--begin::Aside-->
            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{ asset('user-imgs/plato.jpg')}});">
                <div class="kt-grid__item">
                    <a href="#" class="kt-login__logo">
                        <img src="{{-- asset('assets/media/logos/logo-4.png') --}}">
                    </a>
                </div>
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver">
                    <div class="kt-grid__item kt-grid__item--bottom text-center">
                        <h3 class="kt-login__title">{{ __('global.showcase.title') }}</h3>
                        <h4 class="kt-login__subtitle">{{ __('global.showcase.subtitle') }}</h4>
                    </div>
                </div>
                <div class="kt-grid__item">
                    <div class="kt-login__info">
                        <div class="kt-login__copyright">
                            {{ __('global.footer.copyright', ['company'=> config('app.name', 'Law Firm')]) }}
                        </div>
                        <div class="kt-login__menu">
                            <a href="#" class="kt-link">{{ __('global.footer.about') }}</a>
                            <a href="#" class="kt-link">{{ __('global.footer.team') }}</a>
                            <a href="#" class="kt-link">{{ __('global.footer.contact') }}</a>
                        </div>
                    </div>
                </div>
            </div>

            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper">

                <!--begin::Body-->
                <div class="kt-login__body">

                    <!--begin::Signin-->
                    <div class="kt-login__form">
                        <div class="kt-login__title">
                            <h3>@yield('form-title')</h3>
                        </div>

                        <div class="kt-section__content kt-section__content--solid">
                            <div class="kt-divider">
                                <span></span>
                                <span>{{ __('global.lang.choose') }}</span>
                                <span></span>
                            </div>
                        </div>

                        <div class="text-center" style="padding-top: 20px">
                            <div class="kt-media kt-media--circle" style="padding: 0px 20px">
                                <a href="{{ url('change-language/en')}}">
                                    <img src="{{ asset('assets/media/flags/020-flag.svg') }}" alt="image">
                                </a>
                                @if(Config::get('app.locale') === 'en') <i class="flaticon2-correct kt-font-success" style="margin-left: -10px"></i> @endif
                            </div>

                            <div class="kt-media kt-media--circle" style="padding: 0px 20px">
                                <a href="{{ url('change-language/tr')}}">
                                    <img src="{{ asset('assets/media/flags/006-turkey.svg') }}" alt="image">
                                </a>
                                @if(Config::get('app.locale') === 'tr') <i class="flaticon2-correct kt-font-success" style="margin-left: -10px"></i> @endif
                            </div>

                            <div class="kt-media kt-media--circle {{--kt-media--sm--}}" style="padding: 0px 20px">
                                <a href="{{ url('change-language/el')}}">
                                    <img src="{{ asset('assets/media/flags/021-greece.svg') }}" alt="image">
                                </a>
                                @if(Config::get('app.locale') === 'el') <i class="flaticon2-correct kt-font-success" style="margin-left: -10px"></i> @endif
                            </div>
                        </div>

                        <div class="kt-separator"></div>
                        <!--begin::Form-->
                        @if ($errors->has('email'))
                            <p class="kt-font-danger text-center" style="padding-top: 10px">ERROR: {{ $errors->first('email') }}</p>
                        @endif
                        @yield('auth-form')
                        <!--end::Form-->
                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>

<!-- end:: Page -->


<!--begin::Global Theme Bundle(used by all pages) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

</body>

<!-- end::Body -->
</html>