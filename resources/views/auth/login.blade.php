@extends('auth.layouts.master')

@section('form-title', __('auth.lbl_sign_in') )

@section('auth-form')
    <form class="kt-form" id="kt_login_form" method="POST" action="{{ url('/login') }}">
        {{ csrf_field() }}
        <div class="form-group">
            <input class="form-control" type="email" placeholder="{{ __('auth.email_address') }}" name="email" required>
        </div>

        <div class="form-group">
            <input class="form-control" type="password" placeholder="{{ __('auth.password') }}" name="password" required>
        </div>

    <!--begin::Action-->
        <div class="kt-login__actions">
            <a href="{{ url('/password/reset') }}" class="kt-link kt-login__link-forgot">
                {{ __('auth.btn_reset_password') }}
            </a>
            <button
                    type="submit"
                    class="btn btn-primary btn-elevate kt-login__btn-primary">
                {{ __('auth.btn_sign_in') }}
            </button>
        </div>
    </form>
@endsection