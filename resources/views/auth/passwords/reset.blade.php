@extends('auth.layouts.master')

@section('form-title', __('auth.lbl_reset_password'))

@section('auth-form')
    <form class="kt-form" id="kt_login_form" method="POST" action="{{ route('password.email') }}">
        {{ csrf_field() }}

        <div class="form-group row">
            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('auth.email_address') }}</label>

            <div class="col-md-6">
                <input id="email" type="email" class="form-control" name="email" required autocomplete="email" autofocus>

                @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <!--begin::Action-->
        <div class="kt-login__actions">
            <a href="{{ url('/login') }}" class="kt-link kt-login__link-forgot">
                {{ __('auth.btn_want_login') }}
            </a>
            <button
                    type="submit"
                    class="btn btn-primary btn-elevate kt-login__btn-primary">
                {{ __('auth.btn_recover') }}
            </button>
        </div>
    </form>
@endsection