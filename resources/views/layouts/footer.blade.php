<div class="kt-footer  kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
    <div class="kt-container  kt-container--fluid ">
        <div class="kt-footer__copyright">
            {{ __('global.footer.copyright', ['company'=> config('app.name', 'Law Firm')]) }}
        </div>
        <div class="kt-footer__menu">
            <a href="" target="_blank" class="kt-footer__menu-link kt-link">{{ __('global.footer.about') }}</a>
            <a href="" target="_blank" class="kt-footer__menu-link kt-link">{{ __('global.footer.team') }}</a>
            <a href="" target="_blank" class="kt-footer__menu-link kt-link">{{ __('global.footer.contact') }}</a>
        </div>
    </div>
</div>