<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
            {{ __('accounts.btn_export_transactions') }}
        </a>
        <a href="{{ route('new-account') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-account' ? 'active' : '' }}">
            {{ __('accounts.btn_new_account') }}
        </a>
    </div>
</div>