@extends('pages.accounts.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon-piggy-bank"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('accounts.menu_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('accounts.tbl_acc_name') }}</th>
                    <th>{{ __('accounts.tbl_currency') }}</th>
                    <th>{{ __('accounts.tbl_balance') }}</th>
                    <th>{{ __('accounts.tbl_default') }}</th>
                    <th>{{ __('accounts.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Starter Account</td>
                    <td>USD</td>
                    <td>2,000,000.00</td>
                    <td>YES</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection