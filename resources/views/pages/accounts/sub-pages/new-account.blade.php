@extends('pages.accounts.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    New Bank Account
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{ route('accounts-main') }}" class="btn btn-brand btn-pill kt-margin-r-10">
                    <i class="la la-home"></i>
                    <span class="kt-hidden-mobile">Accounts Home</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            Tasks form input here
        </div>
        <div class="kt-portlet__foot">
            <div class="row align-items-right">
                <div class="col-lg-12 kt-align-right">
                    <button type="submit" class="btn btn-brand btn-sm">Save</button>
                    <button type="button" class="btn btn-default btn-sm">Cancel</button>
                </div>
            </div>
        </div>
    </div>
@endsection