<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">{{ __('activities.menu_title') }}</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <a href="{{ route('activities-all', []) }}" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('activities.btn_all') }}
    </a>

    <a href="#" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('activities.btn_time') }}
    </a>

    <a href="#" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('activities.btn_expense') }}
    </a>
</div>