<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="{{ route('activities-categories', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='activities-categories' ? 'active' : '' }}">
            {{ __('activities.btn_manage') }}
        </a>

        <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
            {{ __('activities.btn_new_time') }}
        </a>

        <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
            {{ __('activities.btn_new_expense') }}
        </a>
    </div>
</div>