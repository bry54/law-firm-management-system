@extends('layouts.master')

@section('page-title', __('activities.menu_title'))

@section('header-left')
    @include('pages.activities.header-left')
@endsection

@section('header-right')
    @include('pages.activities.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection