@extends('pages.activities.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-bell"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('activities.menu_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-brand btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> {{ __('activities.btn_export') }}
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">{{ __('activities.lbl_choose') }}</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">{{ __('activities.lbl_print') }}</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">{{ __('activities.lbl_excel') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('activities.tbl_type') }}</th>
                    <th>{{ __('activities.tbl_quantity') }}</th>
                    <th>{{ __('activities.tbl_description') }}</th>
                    <th>{{ __('activities.tbl_matter') }}</th>
                    <th>{{ __('activities.tbl_rate') }}</th>
                    <th>{{ __('activities.tbl_billable') }}</th>
                    <th>{{ __('activities.tbl_non_billable') }}</th>
                    <th>{{ __('activities.tbl_date') }}</th>
                    <th>{{ __('activities.tbl_user') }}</th>
                    <th>{{ __('activities.tbl_invoice_status') }}</th>
                    <th>{{ __('activities.tbl_expense_category') }}</th>
                    <th>{{ __('activities.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                            <i class="la la-bank"></i>
                        </a>
                    </td>
                    <td>10</td>
                    <td>Stationary</td>
                    <td>Family</td>
                    <td>$8/piece</td>
                    <td>30 minutes</td>
                    <td>1 hour</td>
                    <td>10 Oct 2019</td>
                    <td>Someting Wong</td>
                    <td>Paid</td>
                    <td>Acqusition</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>
                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                            <i class="la la-user"></i>
                        </a>
                    </td>
                    <td>10</td>
                    <td>Undefined</td>
                    <td>Business</td>
                    <td>$20/piece</td>
                    <td>30 minutes</td>
                    <td>1 hour</td>
                    <td>10 Jan 2019</td>
                    <td>Someting Wong Miyou</td>
                    <td>Pending</td>
                    <td>Redist</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection