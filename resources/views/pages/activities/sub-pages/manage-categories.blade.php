@extends('pages.activities.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-menu-4"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('activities.lbl_categories') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-toolbar">
                    <a href="#" class="btn btn-brand btn-elevate btn-bold">
                        <i class="la la-plus"></i>
                        {{ __('activities.btn_add_category') }}
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('activities.tbl_category') }}</th>
                    <th>{{ __('activities.tbl_group_permission') }}</th>
                    <th>{{ __('activities.tbl_co_council') }}</th>
                    <th>{{ __('activities.tbl_rate') }}</th>
                    <th>{{ __('activities.tbl_hourly_flat') }}</th>
                    <th>{{ __('activities.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Family</td>
                    <td>Evryone</td>
                    <td>Yes</td>
                    <td>$8/hr</td>
                    <td>Hourly</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>Corporate</td>
                    <td>Councel</td>
                    <td>No</td>
                    <td>$500</td>
                    <td>Flat</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection