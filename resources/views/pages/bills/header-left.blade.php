<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">{{ __('bills.menu_title') }}</h3>

    <span class="kt-subheader__separator kt-subheader__separator--v"></span>

    <a href="{{ route('bills-all', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ (request()->is('bills/all')) ? 'active' : '' }}">
        {{ __('bills.btn_all') }}
    </a>
    <a href="{{ route('bills-clients', []) }}" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ (request()->is('bills/clients')) ? 'active' : '' }}">
        {{ __('bills.btn_billable') }}
    </a>
    <a href="{{ route('outstanding-balances', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ (request()->is('bills/outstanding-balances')) ? 'active' : '' }}">
        {{ __('bills.btn_outstanding') }}
    </a>
    <a href="{{ route('statement-of-accounts', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ (request()->is('bills/statement-of-accounts')) ? 'active' : '' }}">
        {{ __('bills.btn_statement') }}
    </a>
    <a href="{{ route('export-transactions', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ (request()->is('bills/export-transactions')) ? 'active' : '' }}">
        {{ __('bills.btn_export') }}
    </a>
</div>