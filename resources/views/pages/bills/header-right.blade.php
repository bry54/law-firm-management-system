<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        @yield('additional-items')

        <a href="{{ route('record-payment') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
            {{ __('bills.btn_record_payment') }}
        </a>
    </div>
</div>