@extends('layouts.master')

@section('page-title', __('bills.menu_title'))

@section('header-left')
    @include('pages.bills.header-left')
@endsection

@section('header-right')
    @include('pages.bills.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection