@extends('pages.bills.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-correct"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('bills.lbl_billable_clients') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">

            </div>
        </div>
        <div class="kt-portlet__body">
            <!--start: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('bills.tbl_client') }}</th>
                    <th>{{ __('bills.tbl_unbilled_hours') }}</th>
                    <th>{{ __('bills.tbl_amt_due') }}</th>
                    <th>{{ __('bills.tbl_amt_in_trust') }}</th>
                    <th>{{ __('bills.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>John Wick</td>
                    <td>100</td>
                    <td>$56 000</td>
                    <td>$2 000 000</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-share"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection