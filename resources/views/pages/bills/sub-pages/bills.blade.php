@extends('pages.bills.main')

@section('additional-items')
    <a href="#" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('bills.btn_new_trust') }}
    </a>
@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-correct"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('bills.lbl_all_bills') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="btn-group btn-group btn-group-pill" role="group" aria-label="...">
                    <button type="button" class="btn btn-brand">{{ __('bills.btn_grp_all') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('bills.btn_grp_drafts') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('bills.btn_grp_paid') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('bills.btn_grp_pending') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('bills.btn_grp_awaiting') }}</button>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--start: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('bills.tbl_status') }}</th>
                    <th>{{ __('bills.tbl_client') }}</th>
                    <th>{{ __('bills.tbl_matters') }}</th>
                    <th>{{ __('bills.tbl_due_date') }}</th>
                    <th>{{ __('bills.tbl_issue_date') }}</th>
                    <th>{{ __('bills.tbl_balance') }}</th>
                    <th>{{ __('bills.tbl_paid_on') }}</th>
                    <th>{{ __('bills.tbl_amt_paid') }}</th>
                    <th>{{ __('bills.tbl_type') }}</th>
                    <th>{{ __('bills.tbl_total') }}</th>
                    <th>{{ __('bills.tbl_due_in') }}</th>
                    <th>{{ __('bills.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>panding</td>
                    <td>John Wick</td>
                    <td>Business Fraud</td>
                    <td>2 Weeks</td>
                    <td>10 Feb 2019</td>
                    <td>$2 000 000</td>
                    <td>12 Sept 2019</td>
                    <td>$500 000</td>
                    <td>Transfer</td>
                    <td>$2 500 000</td>
                    <td>2 Weeks</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection