@extends('pages.bills.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-correct"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('bills.lbl_outstanding_balance') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">

            </div>
        </div>
        <div class="kt-portlet__body">
            <!--start: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('bills.tbl_client') }}</th>
                    <th>{{ __('bills.tbl_last_shared') }}</th>
                    <th>{{ __('bills.tbl_last_new_bill_due_date') }}</th>
                    <th>{{ __('bills.tbl_last_paid') }}</th>
                    <th>{{ __('bills.tbl_balance') }}</th>
                    <th>{{ __('bills.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>John Wick</td>
                    <td>10 Jan 2019</td>
                    <td>10 Feb 2020</td>
                    <td>$56 000</td>
                    <td>$2 000 000</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-share"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection