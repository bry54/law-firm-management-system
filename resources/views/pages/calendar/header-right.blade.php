<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="#" class="btn kt-subheader__btn-secondary">{{ \Carbon\Carbon::now()->isoFormat('LLLL') }}</a>
    </div>
</div>