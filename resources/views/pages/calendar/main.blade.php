@extends('layouts.master')

@section('page-title', __('calendar.menu_title'))

@section('header-left')
    @include('pages.calendar.header-left')
@endsection

@section('header-right')
    @include('pages.calendar.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection

@section('page-scripts')
    <script src="{{asset('pages-js/calendar/landing.js')}}" type="text/javascript"></script>
@endsection