@extends('pages.calendar.main')
@section('sub-page')
    <div class="row">
        <div class="col-lg-12">
            <!--begin::Portlet-->
            <div class="kt-portlet" id="kt_portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
												<span class="kt-portlet__head-icon">
													<i class="flaticon-map-location"></i>
												</span>
                        <h3 class="kt-portlet__head-title">
                            {{ __('calendar.menu_title') }}
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <a href="{{ route('new-event') }}" class="btn btn-brand btn-elevate btn-sm {{ request()->route()->getName() === 'new-event' ? 'active' : ''}}">
                            <i class="la la-plus"></i>
                            Add Event
                        </a>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div id="kt_calendar"></div>
                </div>
            </div>
            <!--end::Portlet-->
        </div>
    </div>
@endsection
