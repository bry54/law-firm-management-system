@extends('pages.calendar.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    New Event
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <a href="{{ route('calendar-main') }}" class="btn btn-brand btn-elevate btn-sm">
                    <i class="la la-home"></i>
                    <span class="kt-hidden-mobile">Calendar Home</span>
                </a>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Event Title:</label>
                            <input type="text" class="form-control" placeholder="Event name here">
                        </div>
                        <div class="col-lg-6">
                            <label class="">Event Location:</label>
                            <input type="text" class="form-control" placeholder="Location">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Start Date:</label>
                            <input type="datetime-local" class="form-control" placeholder="Tax Name">
                        </div>
                        <div class="col-lg-6">
                            <label class="">End Date:</label>
                            <input type="datetime-local" class="form-control" placeholder="20.00">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea">Description</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Matter</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select matter</option>
                                <option value="1">Family</option>
                                <option value="1">Business</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Save to this calendar</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select calendar</option>
                                <option value="1">Firm calendar</option>
                                <option value="1">Nelson's calendar</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-7">
                        <button type="reset" class="btn btn-brand">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection