<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="{{ route('new-company-contact') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-company-contact'? 'active' : '' }}">
            {{ __('contacts.btn_new_company') }}
        </a>

        <a href="{{ route('new-person-contact') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-person-contact'? 'active' : '' }}">
            {{ __('contacts.btn_new_person') }}
        </a>
    </div>
</div>