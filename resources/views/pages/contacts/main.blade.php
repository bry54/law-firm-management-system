@extends('layouts.master')

@section('page-title', __('contacts.menu_title') )

@section('header-left')
    @include('pages.contacts.header-left')
@endsection

@section('header-right')
    @include('pages.contacts.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection

@section('page-scripts')
@endsection