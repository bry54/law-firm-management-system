@extends('pages.contacts.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-user"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('contacts.menu_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        <div class="dropdown dropdown-inline">
                            <button type="button" class="btn btn-brand btn-icon-sm dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="la la-download"></i> {{ __('contacts.btn_export') }}
                            </button>
                            <div class="dropdown-menu dropdown-menu-right">
                                <ul class="kt-nav">
                                    <li class="kt-nav__section kt-nav__section--first">
                                        <span class="kt-nav__section-text">{{ __('contacts.lbl_choose') }}</span>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-print"></i>
                                            <span class="kt-nav__link-text">{{ __('contacts.lbl_print') }}</span>
                                        </a>
                                    </li>
                                    <li class="kt-nav__item">
                                        <a href="#" class="kt-nav__link">
                                            <i class="kt-nav__link-icon la la-file-excel-o"></i>
                                            <span class="kt-nav__link-text">{{ __('contacts.lbl_excel') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('contacts.tbl_type') }}</th>
                    <th>{{ __('contacts.tbl_name') }}</th>
                    <th>{{ __('contacts.tbl_phone') }}</th>
                    <th>{{ __('contacts.tbl_email') }}</th>
                    <th>{{ __('contacts.tbl_address') }}</th>
                    <th>{{ __('contacts.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                            <i class="la la-bank"></i>
                        </a>
                    </td>
                    <td>Lecent Groups Of Companies</td>
                    <td>+90 392 000 000 - 1111</td>
                    <td>levent@outgroup.com</td>
                    <td>Gonyeli, Lefkosa, KKT, Turkey</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                            <i class="la la-user"></i>
                        </a>
                    </td>
                    <td>John Wick</td>
                    <td>+90 533 533 3333</td>
                    <td>johnqick@trial.com</td>
                    <td>P Bag 566, Upon Thames, England, UK</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        @include('pages.contacts.modals.add-person')
        @include('pages.contacts.modals.add-company')
    </div>
@endsection