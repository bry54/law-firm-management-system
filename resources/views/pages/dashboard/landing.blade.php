@extends('layouts.master')

@section('page-title', __('dashboard.menu_title') )

@section('header-left')
    @include('pages.dashboard.header-left')
@endsection

@section('header-right')
    @include('pages.dashboard.header-right')
@endsection

@section('content')
    <div>
        <div class="kt-section__content kt-section__content--solid">
            <h3 class="kt-font-transform-u">
                {{ __('dashboard.agenda.main_header') }}
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <!--begin::Portlet-->
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ __('dashboard.agenda.tasks_title') }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ route('new-task') }}" class="btn btn-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                                    {{ __('dashboard.agenda.btn_new_task') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-notification kt-notification--fit">
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                        <input type="checkbox">
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        New order has been received
                                    </div>
                                    <div class="kt-notification__item-time">
                                        2 hrs ago
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                        <input type="checkbox">
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        New customer is registered
                                    </div>
                                    <div class="kt-notification__item-time">
                                        3 hrs ago
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                        <input type="checkbox">
                                        <span></span>
                                    </label>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        Application has been approved
                                    </div>
                                    <div class="kt-notification__item-time">
                                        3 hrs ago
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <!--end::Portlet-->
            </div>

            <div class="col-lg-6">
                <!--Begin::Portlet-->
                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">
                                {{ __('dashboard.agenda.calendar_title') }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-toolbar">
                                <div class="kt-portlet__head-actions">
                                    <a href="{{ route('new-event') }}" class="btn btn-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                                        {{ __('dashboard.agenda.btn_new_event') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <!--Begin::Calendar Events -->
                        <div class="kt-timeline-v3">
                            <div class="kt-timeline-v3__items">
                                <div class="kt-timeline-v3__item kt-timeline-v3__item--info">
                                    <span class="kt-timeline-v3__item-time kt-font-info">09:00</span>
                                    <div class="kt-timeline-v3__item-desc">
																	<span class="kt-timeline-v3__item-text">
																		Contrary to popular belief, Lorem Ipsum is not simply random text.
																	</span><br>
                                        <span class="kt-timeline-v3__item-user-name">
																		<a href="#" class="kt-link kt-link--dark kt-timeline-v3__item-link">
																			By Bob
																		</a>
																	</span>
                                    </div>
                                </div>
                                <div class="kt-timeline-v3__item kt-timeline-v3__item--info">
                                    <span class="kt-timeline-v3__item-time kt-font-warning">10:00</span>
                                    <div class="kt-timeline-v3__item-desc">
																	<span class="kt-timeline-v3__item-text">
																		There are many variations of passages of Lorem Ipsum available.
																	</span><br>
                                        <span class="kt-timeline-v3__item-user-name">
																		<a href="#" class="kt-link kt-link--dark kt-timeline-v3__item-link">
																			By Sean
																		</a>
																	</span>
                                    </div>
                                </div>
                                <div class="kt-timeline-v3__item kt-timeline-v3__item--info">
                                    <span class="kt-timeline-v3__item-time kt-font-primary">11:00</span>
                                    <div class="kt-timeline-v3__item-desc">
																	<span class="kt-timeline-v3__item-text">
																		Contrary to popular belief, Lorem Ipsum is not simply random text.
																	</span><br>
                                        <span class="kt-timeline-v3__item-user-name">
																		<a href="#" class="kt-link kt-link--dark kt-timeline-v3__item-link">
																			By James
																		</a>
																	</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End::Calendar Events -->
                    </div>
                </div>

                <!--End::Portlet-->
            </div>
        </div>
    </div>

    <div>
        <div class="kt-section__content kt-section__content--solid">
            <h3 class="kt-font-transform-u">
                {{ __('dashboard.metrics.main_header') }}
            </h3>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title kt-font-primary">
                                {{ __('dashboard.metrics.billable_hours_title') }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                                <a href="{{ url('/') }}" class="btn btn-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
                                    {{ __('dashboard.metrics.btn_setup_target') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body text-center">
                        <p><span class="kt-font-transform-u">{{ __('dashboard.metrics.lbl_no_billing_target') }}</span></p>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title kt-font-primary">
                                {{ __('dashboard.metrics.bill_overview_title') }}
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-actions">
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-section">
                            <div class="kt-section__content kt-section__content--border kt-section__content--fit">
                                <!--begin: Grid Nav -->
                                <div class="kt-grid-nav kt-grid-nav--skin-light">
                                    <div class="kt-grid-nav__row">
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M12.9863016,8.83409843 C12.9953113,8.88805868 13,8.94348179 13,9 L13,11 L17,11 C18.1045695,11 19,11.8954305 19,13 L19,16 L5,16 L5,13 C5,11.8954305 5.8954305,11 7,11 L11,11 L11,9 C11,8.94348179 11.0046887,8.88805868 11.0136984,8.83409843 C9.84135601,8.42615464 9,7.31133193 9,6 C9,4.34314575 10.3431458,3 12,3 C13.6568542,3 15,4.34314575 15,6 C15,7.31133193 14.158644,8.42615464 12.9863016,8.83409843 Z" fill="#000000"/>
                                                                            <rect fill="#000000" opacity="0.3" x="5" y="18" width="14" height="2" rx="1"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_draft_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_draft_subtitle') }}</span>
                                        </a>
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M12,21 C7.581722,21 4,17.418278 4,13 C4,8.581722 7.581722,5 12,5 C16.418278,5 20,8.581722 20,13 C20,17.418278 16.418278,21 12,21 Z" fill="#000000" opacity="0.3"/>
                                                                            <path d="M13,5.06189375 C12.6724058,5.02104333 12.3386603,5 12,5 C11.6613397,5 11.3275942,5.02104333 11,5.06189375 L11,4 L10,4 C9.44771525,4 9,3.55228475 9,3 C9,2.44771525 9.44771525,2 10,2 L14,2 C14.5522847,2 15,2.44771525 15,3 C15,3.55228475 14.5522847,4 14,4 L13,4 L13,5.06189375 Z" fill="#000000"/>
                                                                            <path d="M16.7099142,6.53272645 L17.5355339,5.70710678 C17.9260582,5.31658249 18.5592232,5.31658249 18.9497475,5.70710678 C19.3402718,6.09763107 19.3402718,6.73079605 18.9497475,7.12132034 L18.1671361,7.90393167 C17.7407802,7.38854954 17.251061,6.92750259 16.7099142,6.53272645 Z" fill="#000000"/>
                                                                            <path d="M11.9630156,7.5 L12.0369844,7.5 C12.2982526,7.5 12.5154733,7.70115317 12.5355117,7.96165175 L12.9585886,13.4616518 C12.9797677,13.7369807 12.7737386,13.9773481 12.4984096,13.9985272 C12.4856504,13.9995087 12.4728582,14 12.4600614,14 L11.5399386,14 C11.2637963,14 11.0399386,13.7761424 11.0399386,13.5 C11.0399386,13.4872031 11.0404299,13.4744109 11.0414114,13.4616518 L11.4644883,7.96165175 C11.4845267,7.70115317 11.7017474,7.5 11.9630156,7.5 Z" fill="#000000"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_awaiting_payment_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_awaiting_payment_subtitle') }}</span>
                                        </a>
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M3.5,3 L5,3 L5,19.5 C5,20.3284271 4.32842712,21 3.5,21 L3.5,21 C2.67157288,21 2,20.3284271 2,19.5 L2,4.5 C2,3.67157288 2.67157288,3 3.5,3 Z" fill="#000000"/>
                                                                            <path d="M6.99987583,2.99995344 L19.754647,2.99999303 C20.3069317,2.99999474 20.7546456,3.44771138 20.7546439,3.99999613 C20.7546431,4.24703684 20.6631995,4.48533385 20.497938,4.66895776 L17.5,8 L20.4979317,11.3310353 C20.8673908,11.7415453 20.8341123,12.3738351 20.4236023,12.7432941 C20.2399776,12.9085564 20.0016794,13 19.7546376,13 L6.99987583,13 L6.99987583,2.99995344 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_overdue_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_overdue_subtitle') }}</span>
                                        </a>
                                    </div>
                                    <div class="kt-grid-nav__row">
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--success kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M16.0322024,5.68722152 L5.75790403,15.945742 C5.12139076,16.5812778 5.12059836,17.6124773 5.75613416,18.2489906 C5.75642891,18.2492858 5.75672377,18.2495809 5.75701875,18.2498759 L5.75701875,18.2498759 C6.39304347,18.8859006 7.42424328,18.8859006 8.060268,18.2498759 C8.06056298,18.2495809 8.06085784,18.2492858 8.0611526,18.2489906 L18.3196731,7.9746922 C18.9505124,7.34288268 18.9501191,6.31942463 18.3187946,5.68810005 L18.3187946,5.68810005 C17.68747,5.05677547 16.6640119,5.05638225 16.0322024,5.68722152 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <path d="M9.85714286,6.92857143 C9.85714286,8.54730513 8.5469533,9.85714286 6.93006028,9.85714286 C5.31316726,9.85714286 4,8.54730513 4,6.92857143 C4,5.30983773 5.31316726,4 6.93006028,4 C8.5469533,4 9.85714286,5.30983773 9.85714286,6.92857143 Z M20,17.0714286 C20,18.6901623 18.6898104,20 17.0729174,20 C15.4560244,20 14.1428571,18.6901623 14.1428571,17.0714286 C14.1428571,15.4497247 15.4560244,14.1428571 17.0729174,14.1428571 C18.6898104,14.1428571 20,15.4497247 20,17.0714286 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_draft_total_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_draft_total_subtitle') }}</span>
                                        </a>
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--warning kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M16.0322024,5.68722152 L5.75790403,15.945742 C5.12139076,16.5812778 5.12059836,17.6124773 5.75613416,18.2489906 C5.75642891,18.2492858 5.75672377,18.2495809 5.75701875,18.2498759 L5.75701875,18.2498759 C6.39304347,18.8859006 7.42424328,18.8859006 8.060268,18.2498759 C8.06056298,18.2495809 8.06085784,18.2492858 8.0611526,18.2489906 L18.3196731,7.9746922 C18.9505124,7.34288268 18.9501191,6.31942463 18.3187946,5.68810005 L18.3187946,5.68810005 C17.68747,5.05677547 16.6640119,5.05638225 16.0322024,5.68722152 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <path d="M9.85714286,6.92857143 C9.85714286,8.54730513 8.5469533,9.85714286 6.93006028,9.85714286 C5.31316726,9.85714286 4,8.54730513 4,6.92857143 C4,5.30983773 5.31316726,4 6.93006028,4 C8.5469533,4 9.85714286,5.30983773 9.85714286,6.92857143 Z M20,17.0714286 C20,18.6901623 18.6898104,20 17.0729174,20 C15.4560244,20 14.1428571,18.6901623 14.1428571,17.0714286 C14.1428571,15.4497247 15.4560244,14.1428571 17.0729174,14.1428571 C18.6898104,14.1428571 20,15.4497247 20,17.0714286 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_awaiting_payment_total_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_awaiting_payment_total_subtitle') }}</span>
                                        </a>
                                        <a href="#" class="kt-grid-nav__item">
																<span class="kt-grid-nav__icon">
																	<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon kt-svg-icon--danger kt-svg-icon--lg">
																		<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                                                            <rect x="0" y="0" width="24" height="24"/>
                                                                            <path d="M16.0322024,5.68722152 L5.75790403,15.945742 C5.12139076,16.5812778 5.12059836,17.6124773 5.75613416,18.2489906 C5.75642891,18.2492858 5.75672377,18.2495809 5.75701875,18.2498759 L5.75701875,18.2498759 C6.39304347,18.8859006 7.42424328,18.8859006 8.060268,18.2498759 C8.06056298,18.2495809 8.06085784,18.2492858 8.0611526,18.2489906 L18.3196731,7.9746922 C18.9505124,7.34288268 18.9501191,6.31942463 18.3187946,5.68810005 L18.3187946,5.68810005 C17.68747,5.05677547 16.6640119,5.05638225 16.0322024,5.68722152 Z" fill="#000000" fill-rule="nonzero"/>
                                                                            <path d="M9.85714286,6.92857143 C9.85714286,8.54730513 8.5469533,9.85714286 6.93006028,9.85714286 C5.31316726,9.85714286 4,8.54730513 4,6.92857143 C4,5.30983773 5.31316726,4 6.93006028,4 C8.5469533,4 9.85714286,5.30983773 9.85714286,6.92857143 Z M20,17.0714286 C20,18.6901623 18.6898104,20 17.0729174,20 C15.4560244,20 14.1428571,18.6901623 14.1428571,17.0714286 C14.1428571,15.4497247 15.4560244,14.1428571 17.0729174,14.1428571 C18.6898104,14.1428571 20,15.4497247 20,17.0714286 Z" fill="#000000" opacity="0.3"/>
                                                                        </g>
																	</svg> </span>
                                            <span class="kt-grid-nav__title">{{ __('dashboard.metrics.nav_bill_overdue_total_title') }}</span>
                                            <span class="kt-grid-nav__desc">{{ __('dashboard.metrics.nav_bill_overdue_total_subtitle') }}</span>
                                        </a>
                                    </div>
                                </div>
                                <!--end: Grid Nav -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="hide">
        <div class="kt-section__content kt-section__content--solid">
            <h3 class="kt-font-transform-c">
                Performance Metrics
            </h3>
        </div>

        <div class="row">
        </div>
    </div>--}}

@endsection