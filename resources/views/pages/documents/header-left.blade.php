<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">{{ __('documents.menu_title') }}</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <a href="{{ route('documents-main') }}" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10 {{request()->route()->getName() === 'documents-main' ? 'active' : ''}}">
        {{ __('documents.btn_all_docs') }}
    </a>

    <a href="{{ route('document-categories') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{request()->route()->getName() === 'document-categories' ? 'active' : ''}}">
        {{ __('documents.btn_categories') }}
    </a>

    <a href="{{ url('#') }}" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('documents.btn_templates') }}
    </a>
</div>