@extends('layouts.master')

@section('page-title', __('documents.menu_title'))

@section('header-left')
    @include('pages.documents.header-left')
@endsection

@section('header-right')
    @include('pages.documents.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection

@section('page-scripts')
    <script src="{{asset('pages-js/documents/all-documents.js')}}" type="text/javascript"></script>
@endsection