@extends('pages.documents.main')

@section('sub-page')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    All Documents
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="kt_tree_4" class="tree-demo">
            </div>
        </div>
    </div>
@endsection