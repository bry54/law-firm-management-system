<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">{{ __('matters.menu_title') }}</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <a href="#" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10 active">
        {{ __('matters.btn_all') }}
    </a>

    <a href="#" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('matters.btn_open') }}
    </a>

    <a href="#" class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('matters.btn_pending') }}
    </a>

    <a href="#" class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10">
        {{ __('matters.btn_closed') }}
    </a>
</div>