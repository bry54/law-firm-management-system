<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="{{ route('new-matter') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-matter' ? 'active' : '' }}">
            {{ __('matters.btn_new_matter') }}
        </a>
    </div>
</div>