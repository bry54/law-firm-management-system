@extends('layouts.master')

@section('page-title', __('matters.menu_title') )

@section('header-left')
    @include('pages.matters.header-left')
@endsection

@section('header-right')
    @include('pages.matters.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection