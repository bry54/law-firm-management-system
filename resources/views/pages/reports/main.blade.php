@extends('layouts.master')

@section('page-title', __('reports.menu_title'))

@section('header-left')
    @include('pages.reports.header-left')
@endsection

@section('header-right')
    @include('pages.reports.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection