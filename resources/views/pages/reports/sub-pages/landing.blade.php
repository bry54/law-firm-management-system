@extends('pages.reports.main')

@section('sub-page')
    <style>
        .kt-notification__item-title  {
            font-size: 1.25rem !important;
        }
        .kt-notification__item-time {
            font-size: 1.0rem !important;
        }
    </style>
    <div class="row">
        <div class="col-lg-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.billing_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.billing_reports.acc_receivable_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.billing_reports.acc_receivable_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.billing_reports.aging_acc_receivable_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.billing_reports.aging_acc_receivable_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.billing_reports.billing_history_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.billing_reports.billing_history_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.billing_reports.matter_balance_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.billing_reports.matter_balance_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.billing_reports.invoice_payments_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.billing_reports.invoice_payments_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.client_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.client_activity_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.client_activity_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.client_ledger_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.client_ledger_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.trust_ledger_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.trust_ledger_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.trust_management_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.trust_management_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.trust_listing_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.trust_listing_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.work_in_progress_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.work_in_progress_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.client_reports.client_activity_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.client_reports.client_activity_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.matter_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.matter_reports.matters_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.matter_reports.matters_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.matter_reports.matters_by_responsible_authorities_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.matter_reports.matters_by_responsible_authorities_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.matter_reports.contact_information_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.matter_reports.contact_information_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.productivity_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.productivity_reports.productivity_by_client_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.productivity_reports.productivity_by_client_subtitle') }}
                                </div>
                            </div>
                        </a>
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.productivity_reports.productivity_by_user_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.productivity_reports.productivity_by_user_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            {{ __('reports.revenue_reports.heading') }}
                        </h3>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-notification kt-notification--fit">
                        <a href="#" class="kt-notification__item">
                            <div class="kt-notification__item-icon">
                                -
                            </div>
                            <div class="kt-notification__item-details">
                                <div class="kt-notification__item-title kt-font-transform-c">
                                    {{ __('reports.revenue_reports.revenue_report_title') }}
                                </div>
                                <div class="kt-notification__item-time">
                                    {{ __('reports.revenue_reports.revenue_report_subtitle') }}
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection