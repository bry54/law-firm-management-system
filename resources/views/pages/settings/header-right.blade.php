<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        @if(request()->route()->getName() !== 'settings-main')
            <a href="{{ route('settings-main') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 ">
                <i class="la la-gears"></i>
                <span class="kt-hidden-mobile">Settings Home</span>
            </a>
        @endif
    </div>
</div>