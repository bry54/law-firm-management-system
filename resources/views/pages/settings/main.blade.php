@extends('layouts.master')

@section('page-title', __('settings.menu_title') )

@section('header-left')
    @include('pages.settings.header-left')
@endsection

@section('header-right')
    @include('pages.settings.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection