<!--begin::Modal-->
<div class="modal fade" id="add_person" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">New User </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin::Accordion-->
                <div class="form-group row">
                    <div class="col-lg-6">
                        <label>Email Address:</label>
                        <input type="text" class="form-control" placeholder="user@email.com">
                    </div>
                    <div class="col-lg-6">
                        <label class="">Subscriber Type:</label>
                        <input type="email" class="form-control" placeholder="Enter contact number">
                    </div>
                </div>

                <div class="form-group">
                    <label for="exampleTextarea">Message</label>
                    <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                </div>

                <div class="form-group">
                    <label>Groups</label>
                    <div></div>
                    <select class="custom-select form-control">
                        <option selected="">Select applicable groups</option>
                        <option value="1">Group One</option>
                        <option value="2">Group Two</option>
                        <option value="3">Group Three</option>
                    </select>

                    <div style="padding-top: 10px">
                        <div>
                            <div class="btn-group btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-xm btn-primary">Group</button>
                                <button type="button" class="btn btn-sm btn-danger">x</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="kt-separator"></div>
                <div class="form-group">
                    <label>Permissions</label>
                    <div></div>
                    <select class="custom-select form-control">
                        <option selected="">Select applicable permissions</option>
                        <option value="1">Reports</option>
                        <option value="2">Billing</option>
                        <option value="3">Accounts</option>
                    </select>
                    <div style="padding-top: 10px">
                        <div>
                            <div class="btn-group btn-group" role="group" aria-label="...">
                                <button type="button" class="btn btn-xm btn-primary">Permission</button>
                                <button type="button" class="btn btn-sm btn-danger">x</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Accordion-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save User</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->