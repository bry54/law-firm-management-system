<!--begin::Modal-->
<div class="modal fade" id="add_practice" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalLabel">New Practice</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <div class="modal-body">
                <!--begin::Accordion-->
                <div class="form-group row">
                    <div class="col-lg-12">
                        <label>Practice Title:</label>
                        <input type="text" class="form-control" placeholder="Advocacy">
                    </div>
                </div>
                <!--end::Accordion-->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Practice</button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->