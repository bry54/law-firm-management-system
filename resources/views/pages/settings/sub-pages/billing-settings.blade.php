@extends('pages.settings.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-group"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('settings.firm_settings.billing_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">

            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Primary Tax Name:</label>
                            <input type="text" class="form-control" placeholder="Tax Name">
                        </div>
                        <div class="col-lg-6">
                            <label class="">Primary Tax Rate:</label>
                            <input type="text" class="form-control" placeholder="12.00">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Secondary Tax Name:</label>
                            <input type="text" class="form-control" placeholder="Tax Name">
                        </div>
                        <div class="col-lg-6">
                            <label class="">Secondary Tax Rate:</label>
                            <input type="text" class="form-control" placeholder="20.00">
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Secondary Tax Rule</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select tax rule</option>
                                <option value="1">Apply to pre-tax amount</option>
                                <option value="2">Apply to post-tax amount</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label class="kt-checkbox kt-checkbox--bold kt-checkbox--brand">
                                <input type="checkbox"> Apply Secondary Tax (secondary tax will be applied to bill in addition to primary tax.)
                                <span></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-7">
                        <button type="reset" class="btn btn-brand">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection