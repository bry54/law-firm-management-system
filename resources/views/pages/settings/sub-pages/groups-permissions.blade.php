@extends('pages.settings.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-danger nav-tabs-line-2x nav-tabs-line-right nav-tabs-bold" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_base_demo_2_3_tab_content" role="tab">
                            <i class="fa fa-dolly" aria-hidden="true"></i>Groups
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_2_tab_content" role="tab">
                            <i class="fa fa-shield-alt" aria-hidden="true"></i>Permissions
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_base_demo_2_4_tab_content" role="tab">
                            <i class="fa fa-shield-alt" aria-hidden="true"></i>Job Titles
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="kt_portlet_base_demo_2_3_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Groups
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar" style="padding-top: 10px">
                                <div class="kt-portlet__head-actions">
                                    <a href="#" class="btn btn-outline-brand btn-bold btn-sm">
                                        New Group
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="kt_portlet_base_demo_2_2_tab_content" role="tabpanel">
                    2. tab content
                </div>
                <div class="tab-pane" id="kt_portlet_base_demo_2_4_tab_content" role="tabpanel">
                    <div class="kt-portlet">
                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">
                                    Corporate Titles
                                </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar" style="padding-top: 10px">
                                <div class="kt-portlet__head-actions">
                                    <a href="#" class="btn btn-outline-brand btn-bold btn-sm">
                                        New Title
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__body">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection