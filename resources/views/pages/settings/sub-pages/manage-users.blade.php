@extends('pages.settings.main')

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-group"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('settings.system_settings.manage_users_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <button type="button" class="btn btn-brand kt-margin-r-10" data-toggle="modal" data-target="#add_person">
                    <i class="la la-plus"></i>
                    <span class="kt-hidden-mobile">{{ __('settings.system_settings.btn_new_user') }}</span>
                </button>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--begin: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('settings.system_settings.tbl_title') }}</th>
                    <th>{{ __('settings.system_settings.tbl_fullname') }}</th>
                    <th>{{ __('settings.system_settings.tbl_email') }}</th>
                    <th>{{ __('settings.system_settings.tbl_permissions') }}</th>
                    <th>{{ __('settings.system_settings.tbl_groups') }}</th>
                    <th>{{ __('settings.system_settings.tbl_billing_rate') }}</th>
                    <th>{{ __('settings.system_settings.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Mr</td>
                    <td>Brian Paidamoyo Sithole</td>
                    <td>brianacyth@icloud.com</td>
                    <td>
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">Admin</span>
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">Accounts</span>
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">Reports</span>
                    </td>
                    <td>
                        <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">Group One</span>
                        <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">Group Two</span>
                    </td>
                    <td>$100.00</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>

                <tr>
                    <td>Dr</td>
                    <td>Beverley Brenda Goronga</td>
                    <td>bevsith@gmail.com</td>
                    <td>
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">Accounts</span>
                        <span class="kt-badge kt-badge--danger kt-badge--inline kt-badge--pill kt-badge--rounded">Reports</span>
                    </td>
                    <td>
                        <span class="kt-badge kt-badge--warning kt-badge--inline kt-badge--pill kt-badge--rounded">Group Three</span>
                    </td>
                    <td>$100.00</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-edit"></i>
                                </a>

                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-trash"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
        @include('pages.settings.modals.add-person')
    </div>
@endsection