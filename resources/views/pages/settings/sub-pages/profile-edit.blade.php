@extends('pages.settings.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--tabs">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    {{ __('settings.user_settings.profile_title') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <ul class="nav nav-tabs nav-tabs-bold nav-tabs-line   nav-tabs-line-left nav-tabs-line-brand" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#kt_portlet_tab_1_1" role="tab" aria-selected="false">
                            {{ __('settings.user_settings.tab_your_info') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_2" role="tab" aria-selected="false">
                            {{ __('settings.user_settings.tab_change_login') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_3" role="tab" aria-selected="true">
                            {{ __('settings.user_settings.tab_profile_pic') }}
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_portlet_tab_1_4" role="tab" aria-selected="true">
                            {{ __('settings.user_settings.tab_mail_drop_alias') }}
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="tab-content">
                <div class="tab-pane active" id="kt_portlet_tab_1_1">
                    form 1 here...
                </div>
                <div class="tab-pane" id="kt_portlet_tab_1_2">
                    form  2 here...                </div>
                <div class="tab-pane" id="kt_portlet_tab_1_3">
                    form 3 here...                </div>
                <div class="tab-pane" id="kt_portlet_tab_1_4">
                    form 4 here...                </div>
            </div>
        </div>
    </div>
@endsection