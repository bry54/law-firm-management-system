<div class="kt-subheader__main">
    <h3 class="kt-subheader__title">{{ __('tasks.menu_title') }}</h3>
    <span class="kt-subheader__separator kt-subheader__separator--v"></span>
    <a href="{{ route('upcoming-tasks', []) }}" class="btn btn-label-info btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='upcoming-tasks' ? 'active' : '' }}">
        {{ __('tasks.btn_upcoming') }}
    </a>

    <a href="{{ route('completed-tasks', []) }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='completed-tasks' ? 'active' : '' }}">
        {{ __('tasks.btn_completed') }}
    </a>
</div>