<div class="kt-subheader__toolbar">
    <div class="kt-subheader__wrapper">
        <a href="{{ route('new-task') }}" class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10 {{ request()->route()->getName() ==='new-task' ? 'active' : '' }}">
            {{ __('tasks.btn_new_task') }}
        </a>
    </div>
</div>