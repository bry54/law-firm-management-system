@extends('layouts.master')

@section('page-title', __('tasks.menu_title') )

@section('header-left')
    @include('pages.tasks.header-left')
@endsection

@section('header-right')
    @include('pages.tasks.header-right')
@endsection

@section('content')
    @yield('sub-page')
@endsection