@extends('pages.tasks.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <span class="kt-portlet__head-icon">
                    <i class="flaticon2-writing"></i>
                </span>
                <h3 class="kt-portlet__head-title">
                    New Task
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">

            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="col-lg-6 offset-3">
                    <div class="form-group row">
                        <div class="col-lg-6">
                            <label>Task Name:</label>
                            <input type="text" class="form-control" placeholder="Task name here">
                        </div>
                        <div class="col-lg-6">
                            <label>Matter:</label>
                            <input type="text" class="form-control" placeholder="Matter number here">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea">Description</label>
                        <textarea class="form-control" id="exampleTextarea" rows="3"></textarea>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Priority</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select priority</option>
                                <option value="1">High</option>
                                <option value="1">Normal</option>
                                <option value="1">Low</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Task Type</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select type</option>
                                <option value="1">Type One</option>
                                <option value="1">Type Two</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-lg-12">
                            <label>Task Status</label>
                            <div></div>
                            <select class="custom-select form-control">
                                <option selected="">Select status</option>
                                <option value="1">Pending</option>
                                <option value="1">In Progress</option>
                                <option value="1">In Review</option>
                                <option value="1">Completed</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__foot">
            <div class="kt-form__actions">
                <div class="row">
                    <div class="col-lg-5"></div>
                    <div class="col-lg-7">
                        <button type="reset" class="btn btn-brand">Save</button>
                        <button type="reset" class="btn btn-secondary">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection