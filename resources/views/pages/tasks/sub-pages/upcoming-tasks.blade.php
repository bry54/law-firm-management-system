@extends('pages.tasks.main')

@section('additional-items')

@endsection

@section('sub-page')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
										<span class="kt-portlet__head-icon">
											<i class="kt-font-brand flaticon2-list-1"></i>
										</span>
                <h3 class="kt-portlet__head-title">
                    {{ __('tasks.title_upcoming_tasks') }}
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="btn-group btn-group btn-group-pill" role="group" aria-label="...">
                    <button type="button" class="btn btn-brand active">{{ __('tasks.btn_grp_all') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('tasks.btn_grp_this_wk') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('tasks.btn_grp_today') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('tasks.btn_grp_tomorrow') }}</button>
                    <button type="button" class="btn btn-brand">{{ __('tasks.btn_grp_overdue') }}</button>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <!--start: Datatable -->
            <table class="table table-striped- table-bordered table-hover table-checkable" id="kt_table_1">
                <thead>
                <tr>
                    <th>{{ __('tasks.tbl_due_date') }}</th>
                    <th>{{ __('tasks.tbl_task') }}</th>
                    <th>{{ __('tasks.tbl_description') }}</th>
                    <th>{{ __('tasks.tbl_matter') }}</th>
                    <th>{{ __('tasks.tbl_actions') }}</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>10 Feb 2020</td>
                    <td>Gather Evidence</td>
                    <td>Gather wiki leaks facts for accusing Mr Mbeki</td>
                    <td>Family</td>
                    <td data-field="Actions" data-autohide-disabled="false" class="kt-datatable__cell">
                        <span style="overflow: visible; position: relative; width: 110px;">
                            <div class="dropdown">
                                <a data-toggle="dropdown" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                    <i class="la la-check-square"></i>
                                </a>
                            </div>
                        </span>
                    </td>
                </tr>
                </tbody>
            </table>
            <!--end: Datatable -->
        </div>
    </div>
@endsection