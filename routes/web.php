<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('change-language/{languageCode}', 'Web\LanguageController@change');

Route::middleware(['web','auth'])->group(function () {
    Route::get('/', function () {
        return redirect()->to('/dashboard');
    })->name('index');

    Route::get('/back', function () {
        return back()->with('hide_back',true);
    })->name('portlet-back');

    Route::get('/dashboard', 'Web\Dashboard\MainController@index')->name('dashboard');;

    Route::get('/calendar/all', 'Web\Calendar\MainController@index')->name('calendar-main');
    Route::get('/calendar/new-event', 'Web\Calendar\MainController@indexNewEvent')->name('new-event');

    Route::get('/tasks/upcoming', 'Web\Tasks\UpcomingTasksController@index')->name('upcoming-tasks');
    Route::get('/tasks/completed', 'Web\Tasks\CompletedTasksController@index')->name('completed-tasks');
    Route::get('/tasks/new-task', 'Web\Tasks\MainController@index')->name('new-task');

    Route::get('/matters/all', 'Web\Matters\MainController@index')->name('matters-main');
    Route::get('/matters/new-matter', 'Web\Matters\MainController@indexNewMatter')->name('new-matter');

    Route::get('/contacts/all', 'Web\Contacts\MainController@index')->name('contacts-main');
    Route::get('/contacts/new-person', 'Web\Contacts\MainController@indexNewPerson')->name('new-person-contact');
    Route::get('/contacts/new-company', 'Web\Contacts\MainController@indexNewCompany')->name('new-company-contact');

    Route::get('/activities/all', 'Web\Activities\MainController@index')->name('activities-all');
    Route::get('/activities/categories', 'Web\Activities\CategoriesController@index')->name('activities-categories');

    Route::get('/bills/all', 'Web\Bills\MainController@index')->name('bills-all');
    Route::get('/bills/record-payment', 'Web\Bills\MainController@indexRecordPayment')->name('record-payment');
    Route::get('/bills/clients', 'Web\Bills\BillableClientsController@index')->name('bills-clients');
    Route::get('/bills/export-transactions', 'Web\Bills\ExportTransactionsController@index')->name('export-transactions');
    Route::get('/bills/outstanding-balances', 'Web\Bills\OutstandingBalancesController@index')->name('outstanding-balances');
    Route::get('/bills/statement-of-accounts', 'Web\Bills\StatementOfAccountsController@index')->name('statement-of-accounts');

    Route::get('/accounts/all', 'Web\Accounts\MainController@index')->name('accounts-main');
    Route::get('/accounts/new', 'Web\Accounts\MainController@indexNewAccount')->name('new-account');

    Route::get('/documents/all', 'Web\Documents\MainController@index')->name('documents-main');
    Route::get('/documents/categories', 'Web\Documents\MainController@indexCategories')->name('document-categories');

    Route::get('/reports', 'Web\Reports\MainController@index')->name('reports-main');

    Route::get('/settings/all', 'Web\Settings\MainController@index')->name('settings-main');
    Route::get('/settings/edit-profile', 'Web\Settings\MainController@indexProfileEdit')->name('edit-profile');
    Route::get('/settings/practice-areas', 'Web\Settings\MainController@indexPractiseArea')->name('practise-areas');
    Route::get('/settings/billing', 'Web\Settings\MainController@indexBilling')->name('billing-settings');
    Route::get('/settings/users', 'Web\Settings\MainController@indexUsers')->name('manage-users');
    Route::get('/settings/groups-permissions', 'Web\Settings\MainController@indexGroupPermissions')->name('groups-permissions');

    Route::get('/help', 'Web\Help\MainController@index')->name('help-main');
});

Auth::routes(['register' => false]);
